package com.dwestgate.earthquaketracker

class Quake(datetime: String, depth: Double, lng: Double, src: String, eqid: String, magnitude: Double, lat: Double) {
    private  var datetime: String = datetime
    fun getdatetime(): String{
        return datetime
    }
    private var depth: Double = depth
    fun getdepth(): Double{
        return depth
    }
    private var lng: Double = lng
    fun getlng(): Double{
        return lng
    }
    private var lat: Double = lat
    fun getlat(): Double{
        return lat
    }
    private var src: String = src
    fun getsrc(): String{
        return src
    }
    private var magnitude: Double = magnitude
    fun getmagnitude(): Double{
        return magnitude
    }
    private var eqid: String = eqid
    fun geteqid(): String{
        return eqid
    }
    override fun toString(): String {
        return "EarthQuake(datetime='$datetime', depth='$depth', lng='$lng', lat='$lat', src='$src', magnitude='$magnitude',eqid='$eqid')";
    }
}