package com.dwestgate.earthquaketracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    val TAG: String = "DJW"; //Logger & Debug

    private lateinit var mMap: GoogleMap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**Generated Comment:
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val lat: Double = intent.extras?.getDouble("lat") ?: 0.0;
        val lng: Double = intent.extras?.getDouble("lng") ?: 0.0;

        /*If we want to include more data in this activity later
        //val eqid : String? = intent.extras?.getString("eqid")?:"";
        //val datetime : String? = intent.extras?.getString("datetime")?:""; */

        val quakeGPS = LatLng(lat, lng)
        mMap.addMarker(MarkerOptions().position(quakeGPS).title("GPS: ${lat}, ${lng}"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(quakeGPS))
    }
}