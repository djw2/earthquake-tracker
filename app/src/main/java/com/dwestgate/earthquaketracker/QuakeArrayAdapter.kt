package com.dwestgate.earthquaketracker

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class QuakeArrayAdapter(context: Context, private val layoutResource: Int, private val quakes: ArrayList<Quake>) :
    ArrayAdapter<Quake>(context, layoutResource) {

    override fun getCount(): Int {
        if (quakes == null) {
            return 0
        }
        else{
            return quakes.size;
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val context = parent.context;
        val quake = quakes[position]
        val view: View;
        if(convertView == null) {
            view = LayoutInflater.from(context).inflate(layoutResource, parent, false)
        }
        else{
            view = convertView;
        }
        val magnitude_view : TextView = view.findViewById(R.id.magnitude_view) as TextView;
        val magnitude : Double = quake.getmagnitude()
        magnitude_view.text = magnitude.toString()
        val datetime_view : TextView = view.findViewById(R.id.datetime_view) as TextView;
        datetime_view.text = "Date & Time: ${quake.getdatetime()}"
        val eqid_view : TextView = view.findViewById(R.id.eqid_view) as TextView;
        eqid_view.text = "Eq Id: ${quake.geteqid()}"
        val depth_view : TextView = view.findViewById(R.id.depth_view) as TextView;
        depth_view.text = "${quake.getdepth()}km"
        val gps_view : TextView = view.findViewById(R.id.gps_view) as TextView;
        gps_view.text = "GPS: ${quake.getlat()}, ${quake.getlng()}";

        if(magnitude_view.currentTextColor== Color.WHITE && magnitude >= 8.0){
            magnitude_view.setTextColor(Color.RED);
        }
        else if (magnitude_view.currentTextColor== Color.RED && magnitude < 8.0){
            magnitude_view.setTextColor(Color.WHITE)
        }
        return view
    }
}