package com.dwestgate.earthquaketracker

import android.content.Intent
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.net.HttpURLConnection
import java.net.URL

const val TAG: String = "DJW"; //For log & debug


class MainActivity : AppCompatActivity() {
    lateinit var listView: ListView;
    lateinit var quakes: ArrayList<Quake>;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.list_view);

        //Begin coroutines
        CoroutineScope(Dispatchers.IO).launch {

            //Initial IO Dispatcher for web requests
            val jsonString = getJSONFromWeb()

            //Default Dispatcher for work on objects
            withContext(Dispatchers.Default) {
                initQuakeList(jsonString);
            }

            //Main for work on UI
            withContext(Dispatchers.Main) {
                populateView();
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    //Initializes & populate earthquake arraylist from json formatted string
    private fun initQuakeList(jsonString: String) {

        val jsonSrc = JSONObject(jsonString);
        val jsonArray = jsonSrc.getJSONArray("earthquakes")

        /* Alternate approach using Gson library (3rd party?)
        val quakeArray = object : TypeToken<ArrayList<Quake>>() {}.type;
        val gson = Gson();
        quakes = gson.fromJson(jsonArray, quakeArray);*/

        val length = jsonArray.length();
        quakes = arrayListOf<Quake>()

        var i = 0;
        while (i < length) {
            val jsonObject: JSONObject = jsonArray.get(i) as JSONObject;
            val datetime: String = jsonObject.getString("datetime")
            val depth: Double = jsonObject.getDouble("depth")
            val lng: Double = jsonObject.getDouble("lng")
            val lat: Double = jsonObject.getDouble("lat")
            val src: String = jsonObject.getString("src")
            val magnitude: Double = jsonObject.getDouble("magnitude")
            val eqid: String = jsonObject.getString("eqid")
            val nextQuake: Quake = Quake(datetime, depth, lng, src, eqid, magnitude, lat)
            quakes.add(nextQuake);
            i++
        }
    }

    //Sets layout adapter to display earthquake data
    private fun populateView() {
        val adapter = QuakeArrayAdapter(this, R.layout.earthquake_row, quakes);
        adapter.addAll(quakes);
        listView.adapter = adapter;
        listView.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, MapsActivity::class.java);
            intent.putExtra("lng", quakes[position].getlng());
            intent.putExtra("lat", quakes[position].getlat());
            intent.putExtra("datetime", quakes[position].getdatetime());
            intent.putExtra("eqid", quakes[position].geteqid());
            startActivity(intent)
        }
    }

    //Performs http request on IO coroutine and returns string of data
    private suspend fun getJSONFromWeb(): String {
        val url = URL(getString(R.string.web_resource));
        var inputStream: InputStream;
        var value = "";
        withContext(Dispatchers.IO) {
            val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection;
            try {
                inputStream = BufferedInputStream(urlConnection.inputStream);
                value += readStream(inputStream, 10000);
            } finally {
                urlConnection.disconnect();
            }
        }
        return value;
    }

    //Read input stream and return string
    //Source: https://developer.android.com/training/basics/network-ops/connecting
    private fun readStream(stream: InputStream, maxReadSize: Int): String {
        val reader: Reader = InputStreamReader(stream, "UTF-8");
        val rawBuffer = CharArray(maxReadSize)
        val buffer = StringBuffer()
        var readSize: Int = reader.read(rawBuffer)
        var maxReadBytes = maxReadSize
        while (readSize != -1 && maxReadBytes > 0) {
            if (readSize > maxReadBytes) {
                readSize = maxReadBytes
            }
            buffer.append(rawBuffer, 0, readSize)
            maxReadBytes -= readSize
            readSize = reader.read(rawBuffer)
        }
        return buffer.toString();
    }
}

